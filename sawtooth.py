import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
t = np.linspace(0, 1, 500)
plt.figure(figsize=(20,10))
# for first program
plt.plot(t*1000, signal.sawtooth(2 * np.pi * 10 * t)) 

#for second program
#plt.plot(t*1000, signal.sawtooth(2 * np.pi * 5 * t))

plt.title('Sawtooth Signal',fontsize=20)
plt.xlabel('time(ms)',fontsize=18)
plt.ylabel('Amplitude',fontsize=18)
plt.yticks([-2,-1.5,-1,-0.5,0,0.5,1,1.5,2])
plt.ylim(-2,2)
plt.xlim(0,1000)
plt.grid()
plt.show()


