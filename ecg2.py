import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
y= (genfromtxt('ecg_data.csv', delimiter=',')-350)/50
t = np.linspace(0, 1, len(y), endpoint=False)

plt.plot(t[0:400]*20,y[0:400]/5)



plt.xticks([0,0.5,1,1.5,2,2.5,3])


plt.title('ECG Signal',fontsize=20)
plt.xlabel('time(sec)',fontsize=18)
plt.ylabel('Amplitude(mV)',fontsize=18)

plt.yticks([-2,-1.5,-1,-0.5,0,0.5,1,1.5,2])
plt.ylim(0,2)
plt.xlim(0,3)
plt.grid()
plt.show()
plt.show()



